﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopOnline.Models
{
    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
  
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class UpdateInfo
    {
        [Display(Name = "Full name")]
        public string FullName { get; set; }
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email không đúng định dạng.")]
        public string Email { get; set; }
        [Display(Name = "Mobile Number")]
        [RegularExpression("(01([0-9]{9}))|(09([0-9]{8}))", ErrorMessage = "10 số đối với sđt bắt đầu 09, 11 số với bắt đầu 01")]
        public string MobileNumber { get; set; }
    }
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Không được để trống.")]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Không được để trống.")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email không đúng định dạng.")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Không được để trống.")]
        [Display(Name = "Mobile Number")]
        [RegularExpression("(01([0-9]{9}))|(09([0-9]{8}))", ErrorMessage = "10 số đối với sđt bắt đầu 09, 11 số với bắt đầu 01")]
        public string MobileNumber { get; set; }
    }
    public class UserAndRole
    {
        public string UserName { get; set; }
        public string CreateDate { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
    }
    public class InfoUsers
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Full name")]
        public string FullName { get; set; }
        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email không đúng định dạng.")]
        public string Email { get; set; }
        [Required]
        [RegularExpression("(01([0-9]{9}))|(09([0-9]{8}))", ErrorMessage = "10 số đối với sđt bắt đầu 09, 11 số với bắt đầu 01")]
        public string NumberPhone { get; set; }
        
        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}
