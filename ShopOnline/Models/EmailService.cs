﻿using System.Net;
using System.Net.Mail;
using System.Text;

namespace ShopOnline.Models
{
    public class EmailService
    {
        /// <summary>
        /// Hàm thực thi gửi email kích hoạt tài khoản
        /// </summary>
        /// <param name="smtpUserName">Ten dang nhap cua smtp  service</param>
        /// <param name="smtpPassword">Mat khau dang nhap</param>
        /// <param name="smtpHost">SMTP cua host: vd: smtp.gmail.com</param>
        /// <param name="smtpPort">Port cua service: vd: 25-gmail</param>
        /// <param name="toEmail">Email nguoi nhan</param>
        /// <param name="subject">Chu de email</param>
        /// <param name="body">Noi dung thu gui</param>
        /// <returns>true : thanh cong/ false: nguoc lai</returns>
        public bool Send(string smtpUserName, string smtpPassword, string smtpHost,
            int smtpPort, string toEmail, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.EnableSsl = true;//https
                    smtpClient.Host = smtpHost;
                    smtpClient.Port = smtpPort;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(smtpUserName, smtpPassword);
                    var msg = new MailMessage
                    {
                        IsBodyHtml = true,
                        Subject = subject,
                        BodyEncoding = Encoding.UTF8,
                        From = new MailAddress(smtpUserName),
                        Body = body,
                        Priority = MailPriority.Normal,
                    };
                    msg.To.Add(toEmail);
                    smtpClient.Send(msg);
                    return true;
                }
            }
            catch
            {}
            return false;
        }
    }
}