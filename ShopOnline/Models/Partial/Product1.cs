﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ShopOnline.Models
{
    public class Product1
    {
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [Display(Name = "Loại sản phẩm")]
        public Nullable<int> CategoryID { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        [Display(Name = "Số lượng")]
        public Nullable<int> Stock { get; set; }


        [Display(Name = "Giá")]
        public Nullable<int> Price { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public Nullable<System.DateTime> TimeUpdate { get; set; }
    }
}