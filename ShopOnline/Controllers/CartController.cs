﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net;
using ShopOnline.Models;

namespace ShopOnline.Controllers
{
    public class CartController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();
        // GET: Cart
        public ActionResult Index(int? page, string msg, string adress)
        {
            var lsCart = Session["Carts"] as List<Cart>;
            if (lsCart == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            int total = 0;
            foreach(var cart in lsCart)
            {
                total += cart.Product.Price.Value * cart.Quantity;
            }
            ViewBag.total = total;
            ViewBag.carts = lsCart.ToPagedList(pageNumber, pageSize);
            ViewBag.message = msg;
            ViewBag.adress = adress;
            if(Request.IsAjaxRequest())
            {
                return (ActionResult)PartialView("PartialCart");
            }
            return View();
                
        }

        public ActionResult Delete(int id, int? page)
        {
            var lsCart = Session["Carts"] as List<Cart>;
            lsCart.RemoveAt(id);
            return RedirectToAction("Index", new { page = page});
        }

        public ActionResult Update(int id, int value, int? page)
        {
            var lsCart = Session["Carts"] as List<Cart>;
            lsCart[id].Quantity += value;
            if (lsCart[id].Quantity <= 0) return RedirectToAction("Delete", new { id=id});
            return RedirectToAction("Index", new { page = page});
        }

        public ActionResult SetInfo(string adress1, string adress2, string adress3)
        {
            string adress = adress1 +" - "+ adress2 + " - " + adress3;
            return RedirectToAction("Index", new { adress=adress});
        }
        public ActionResult CheckOut(int total, string adress)
        {
            string msg = null;
            if(Request.IsAuthenticated)
            {
                var lsCart = Session["Carts"] as List<Cart>;
                if(lsCart.Count>0)
                {
                    var user = db.AspNetUsers.FirstOrDefault(i => i.UserName == User.Identity.Name);

                    Cart cart = new Cart();
                    Transaction order = new Transaction();

                    order.CustomerID = user.Id;
                    order.CreateDate = DateTime.Now;
                    order.Bill = total;
                    order.Address = adress;
                    order.State = "Chưa giao";
                    db.Transactions.Add(order);
                    db.SaveChanges();

                    foreach (var item in lsCart)
                    {        
                        cart.ProductID = item.Product.ID;
                        cart.TransactionID = order.ID;
                        cart.Quantity = item.Quantity;
                        //
                        Product product = (from p in db.Products
                                           where p.ID == cart.ProductID
                                           select p).First();
                        product.Stock-=cart.Quantity;


                        db.Carts.Add(cart);
                        db.SaveChanges();
                    }

                    lsCart.Clear();
                    msg = "Đơn hàng của bạn đã được xác nhận!";
                }
            }
            else
            {
                msg = "Bạn cần đăng nhập để thực hiện chức năng đặt hàng!";
            }
            return RedirectToAction("Index", new { msg=msg});
        }

    }
}