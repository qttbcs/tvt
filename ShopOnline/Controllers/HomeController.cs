﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;

namespace ShopOnline.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();
        // GET: Home
        [AllowAnonymous]
        public ActionResult Index()
        {
            var lsNew = from p in db.Products
                        where p.NumView==0
                        select p;
            
            Session["New"] = lsNew;
            return View();
        }

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult IndexAdmin()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult Calendar()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult Charts()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult Messages()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Contact(string YourName, string YourMail, string Subject, string Message)
        {
            SendMail(YourName, YourMail, Subject, Message,"thanhvongit@gmail.com");
            return View();
        }
        public bool SendMail(string name,string email,string subject,string body, string mailAdmin)
        {
            string userMailSend = "thanhvongspw@gmail.com";
            string passMailSend = "thanhvongspw01234";
            string BODY = "";
            BODY = "Người gửi: " + name + "<br>Email: " + email + "<br>Nội dung: " + body;
            //doan code gui mail 
            string smtpHost = "smtp.gmail.com";
            int smtpPort = 587;
            EmailService service = new EmailService();
            bool resultSend = service.Send(userMailSend, passMailSend, smtpHost, smtpPort, mailAdmin, subject,BODY);
            if (resultSend) ModelState.AddModelError("", "Send mail successful");
            else ModelState.AddModelError("", "Send mail unsuccessful");
            //
            return resultSend;
        }
        [AllowAnonymous]
        public ActionResult Terms()
        {
            return View();
        }
        
    }
}