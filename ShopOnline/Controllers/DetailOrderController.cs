﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;

namespace ShopOnline.Controllers
{
    [Authorize(Roles = "Admin")]   
    public class DetailOrderController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: DetailOrder
        public ActionResult Index(int? id)
        {
            var carts = from c in db.Carts
                             where c.TransactionID == id
                             select c;
            ViewBag.Transaction = id;
            return View(carts.ToList());
        }


        // GET: DetailOrder/Create
        public ActionResult Create()
        {
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ID");
            ViewBag.TransactionID = new SelectList(db.Transactions, "ID", "CustomerID");
            return View();
        }

        // POST: DetailOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ProductID,TransactionID,Quantity")] Cart cart, int transaction)
        {
            if (ModelState.IsValid)
            {
                cart.TransactionID = transaction;
                db.Carts.Add(cart);
                db.SaveChanges();
                return RedirectToAction("Index", new { id=transaction});
            }

            ViewBag.ProductID = new SelectList(db.Products, "ID", "ID", cart.ProductID);
            ViewBag.TransactionID = new SelectList(db.Transactions, "ID", "CustomerID", cart.TransactionID);
            return View(cart);
        }

        // GET: DetailOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ID", cart.ProductID);
            return View(cart);
        }

        // POST: DetailOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ProductID,TransactionID,Quantity")] Cart cart, int transaction)
        {
            if (ModelState.IsValid)
            {
                cart.TransactionID = transaction;
                db.Entry(cart).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = transaction});
            }
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ID", cart.ProductID);
            return View(cart);
        }

        // GET: DetailOrder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        // POST: DetailOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int transaction)
        {
            Cart cart = db.Carts.Find(id);
            db.Carts.Remove(cart);
            db.SaveChanges();
            return RedirectToAction("Index", new { id=transaction});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
