﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;

namespace ShopOnline.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FashionTypesController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: FashionTypes
        public ActionResult Index()
        {
            return View(db.FashionTypes.ToList());
        }

        // GET: FashionTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FashionTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] FashionType fashionType)
        {
            if (ModelState.IsValid)
            {
                db.FashionTypes.Add(fashionType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fashionType);
        }

        // GET: FashionTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FashionType fashionType = db.FashionTypes.Find(id);
            if (fashionType == null)
            {
                return HttpNotFound();
            }
            return View(fashionType);
        }

        // POST: FashionTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] FashionType fashionType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fashionType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fashionType);
        }

        // GET: FashionTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FashionType fashionType = db.FashionTypes.Find(id);
            if (fashionType == null)
            {
                return HttpNotFound();
            }
            return View(fashionType);
        }

        // POST: FashionTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FashionType fashionType = db.FashionTypes.Find(id);
            db.FashionTypes.Remove(fashionType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
