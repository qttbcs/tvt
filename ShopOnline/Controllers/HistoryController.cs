﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ShopOnline.Models;

namespace ShopOnline.Controllers
{
    public class HistoryController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();
        // GET: History
        public ActionResult Index(int? page)
        {
            var lsHistory = from t in db.Transactions
                         where t.AspNetUser.UserName == User.Identity.Name
                         select t;
            lsHistory = lsHistory.OrderByDescending(t => t.CreateDate);
            //
            int pageSize = 1;
            int pageNumber = (page ?? 1);
            ViewBag.history = lsHistory.ToPagedList(pageNumber, pageSize);
            return Request.IsAjaxRequest()
                ? (ActionResult)PartialView("PartialHistory")
                : View();
        }
    }
}