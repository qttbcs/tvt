﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;
using PagedList;

namespace ShopOnline.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BieuDoQuy1Controller : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index()
        {
            ViewBag.ThongKeTheoQuy = ThongKeTheoQuy();
            ViewBag.Quy = "1";
            return View("~/Views/ThongKeDoanhSo/BieuDoQuy1/Index.cshtml");
        }

        List<int> ThongKeTheoQuy()
        {
            List<int> tkN = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 1; i <= 3; i++)
            {
                tkN.Add(ThongKeTheoThang(i, d.Year));
            }
            return tkN;

        }

        int ThongKeTheoThang(int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (dx.Month == month && dx.Year == year)
                {
                    result += (int)t.Bill;
                }

            }
            return result;
        }
	}

    [Authorize(Roles = "Admin")]
    public class BieuDoQuy2Controller : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index()
        {
            ViewBag.ThongKeTheoQuy = ThongKeTheoQuy();
            ViewBag.Quy = "2";
            return View("~/Views/ThongKeDoanhSo/BieuDoQuy2/Index.cshtml");
        }

        List<int> ThongKeTheoQuy()
        {
            List<int> tkN = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 4; i <= 6; i++)
            {
                tkN.Add(ThongKeTheoThang(i, d.Year));
            }
            return tkN;

        }

        int ThongKeTheoThang(int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (dx.Month == month && dx.Year == year)
                {
                    result += (int)t.Bill;
                }

            }
            return result;
        }
    }

    [Authorize(Roles = "Admin")]
    public class BieuDoQuy3Controller : Controller
    {
        //
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index()
        {
            ViewBag.ThongKeTheoQuy = ThongKeTheoQuy();
            ViewBag.Quy = "3";
            return View("~/Views/ThongKeDoanhSo/BieuDoQuy3/Index.cshtml");
        }

        List<int> ThongKeTheoQuy()
        {
            List<int> tkN = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 7; i <= 9; i++)
            {
                tkN.Add(ThongKeTheoThang(i, d.Year));
            }
            return tkN;

        }

        int ThongKeTheoThang(int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (dx.Month == month && dx.Year == year)
                {
                    result += (int)t.Bill;
                }

            }
            return result;
        }
    }

    [Authorize(Roles = "Admin")]
    public class BieuDoQuy4Controller : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index()
        {
            ViewBag.ThongKeTheoQuy = ThongKeTheoQuy();
            ViewBag.Quy = "4";
            return View("~/Views/ThongKeDoanhSo/BieuDoQuy4/Index.cshtml");
        }

        List<int> ThongKeTheoQuy()
        {
            List<int> tkN = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 10; i <= 12; i++)
            {
                tkN.Add(ThongKeTheoThang(i, d.Year));
            }
            return tkN;

        }

        int ThongKeTheoThang(int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (dx.Month == month && dx.Year == year)
                {
                    result += (int)t.Bill;
                }

            }
            return result;
        }
    }

    [Authorize(Roles = "Admin")]
    public class BieuDoTheoNamController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: /Transaction/
        public ActionResult Index()
        {
            ViewBag.ThongKeTheoNam = ThongKeTheoNam();
            ViewBag.Nam = DateTime.Now.Year.ToString();
            return View("~/Views/ThongKeDoanhSo/BieuDoTheoNam/Index.cshtml");
        }


        List<int> ThongKeTheoNam()
        {
            List<int> tkN = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 1; i <= 12; i++)
            {
                tkN.Add(ThongKeTheoThang(i, d.Year));
            }
            return tkN;

        }

        int ThongKeTheoThang(int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (dx.Month == month && dx.Year == year)
                {
                    result += (int)t.Bill;
                }

            }
            return result;
        }
    }

    [Authorize(Roles = "Admin")]
    public class BieuDoTheoThangController : Controller
    {

        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: /Transaction/
        public ActionResult Index()
        {
            ViewBag.ThongKeTheoThang = ThongKeTheoThang();
            ViewBag.Thang = DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
            return View("~/Views/ThongKeDoanhSo/BieuDoTheoThang/Index.cshtml");
        }


        List<int> ThongKeTheoThang()
        {
            List<int> tkT = new List<int>();
            DateTime d = DateTime.Now;
            tkT.Add(ThongKeTheoTuan(1, 7, d.Month, d.Year));
            tkT.Add(ThongKeTheoTuan(8, 14, d.Month, d.Year));
            tkT.Add(ThongKeTheoTuan(15, 22, d.Month, d.Year));
            tkT.Add(ThongKeTheoTuan(22, -1, d.Month, d.Year));
            return tkT;

        }

        int ThongKeTheoTuan(int x, int y, int month, int year)
        {
            int result = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime dx = Convert.ToDateTime(t.CreateDate);
                if (y != -1)
                {
                    if (dx.Day <= y && dx.Day >= x && dx.Month == month && dx.Year == year)
                    {
                        result += (int)t.Bill;
                    }
                }
                else if (y == -1)
                {
                    if (dx.Day >= x && dx.Month == month && dx.Year == year)
                    {
                        result += (int)t.Bill;
                    }
                }
            }
            return result;

        }



    }

    [Authorize(Roles = "Admin")]
    public class BieuDoTheoTuanController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: /Transaction/
        public ActionResult Index()
        {
            ViewBag.ThongKeTheoTuan = ThongKeTheoTuan();
            return View("~/Views/ThongKeDoanhSo/BieuDoTheoTuan/Index.cshtml");
        }

        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        List<int> ThongKeTheoTuan()
        {
            List<int> tkT = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 0; i < 7; i++)
            {
                tkT.Add(ThongKeTheoNgay(d.AddDays(i)));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
    }

    [Authorize(Roles = "Admin")]
    public class ThongKeTheoNamController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var lstTransaction = from s in db.Transactions
                                 orderby s.ID
                                 select s;
            DateTime dtn = DateTime.Now;
            int year = dtn.Year;
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    year = int.Parse(searchString);
                }
                catch (Exception e)
                {

                }
            }
            List<Transaction> lstT = new List<Transaction>();
            int DoanhThu = 0;
            foreach (Transaction t in lstTransaction)
            {
                DateTime x = Convert.ToDateTime(t.CreateDate);
                if (x.Year == year)
                {
                    DoanhThu += (int)t.Bill;
                    lstT.Add(t);
                }
            }

            ViewBag.ThangThongKe = "Năm " + year.ToString();
            ViewBag.DoanhThu = DoanhThu;
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoNam/Index.cshtml",lstT.ToPagedList(pageNumber, pageSize));
        }


        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        int ThongKeTheoTuan(DateTime input)
        {
            int tkT = 0;
            DateTime d = getMonday(input);
            for (int i = 0; i < 7; i++)
            {
                tkT += ThongKeTheoNgay(d.AddDays(i));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
        ////////////////////////////////////////////////////////////
        // GET: /ThongKe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoNam/Details.cshtml",transaction);
        }

        // GET: /ThongKe/Create
    }

    [Authorize(Roles = "Admin")]
    public class ThongKeTheoNgayController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: /ThongKe/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var lstTransaction = from s in db.Transactions
                                 orderby s.ID
                                 select s;
            DateTime dtn = DateTime.Now;
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    dtn = Convert.ToDateTime(searchString);
                }
                catch (Exception e)
                {

                }
            }
            List<Transaction> lstT = new List<Transaction>();
            foreach (Transaction t in lstTransaction)
            {
                DateTime x = Convert.ToDateTime(t.CreateDate);
                if (dtn.Day == x.Day && dtn.Month == x.Month && dtn.Year == x.Year)
                {
                    lstT.Add(t);
                }
            }
            ViewBag.NgayThongKe = dtn.ToString();
            ViewBag.DoanhThu = ThongKeTheoNgay(dtn);
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoNgay/Index.cshtml",lstT.ToPagedList(pageNumber, pageSize));
        }


        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        List<int> ThongKeTheoTuan()
        {
            List<int> tkT = new List<int>();
            DateTime d = DateTime.Now;
            for (int i = 0; i < 7; i++)
            {
                tkT.Add(ThongKeTheoNgay(d.AddDays(i)));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
        ////////////////////////////////////////////////////////////
        // GET: /ThongKe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoNgay/Details.cshtml", transaction);
        }

        // GET: /ThongKe/Create

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    [Authorize(Roles = "Admin")]
    public class ThongKeTheoQuyController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var lstTransaction = from s in db.Transactions
                                 orderby s.ID
                                 select s;
            int quy = 1;
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    quy = int.Parse(searchString);
                }
                catch (Exception e)
                {

                }
            }
            int month1, month2;
            month1 = month2 = 0;
            switch (quy)
            {
                case 1: month1 = 1; month2 = 3; break;
                case 2: month1 = 4; month2 = 6; break;
                case 3: month1 = 7; month2 = 9; break;
                default:
                    month1 = 10; month2 = 12; break;

            }
            List<Transaction> lstT = new List<Transaction>();
            int DoanhThu = 0;
            foreach (Transaction t in lstTransaction)
            {
                DateTime x = Convert.ToDateTime(t.CreateDate);
                if (x.Month <= month2 && x.Month >= month1 && x.Year == DateTime.Now.Year)
                {
                    DoanhThu += (int)t.Bill;
                    lstT.Add(t);
                }
            }

            ViewBag.QuyThongKe = "Quý " + quy.ToString() + " năm " + DateTime.Now.Year;
            ViewBag.DoanhThu = DoanhThu;
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoQuy/Index.cshtml",lstT.ToPagedList(pageNumber, pageSize));
        }


        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        int ThongKeTheoTuan(DateTime input)
        {
            int tkT = 0;
            DateTime d = getMonday(input);
            for (int i = 0; i < 7; i++)
            {
                tkT += ThongKeTheoNgay(d.AddDays(i));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
        ////////////////////////////////////////////////////////////
        // GET: /ThongKe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoQuy/Details.cshtml",transaction);
        }

        // GET: /ThongKe/Create
    }

    [Authorize(Roles = "Admin")]
    public class ThongKeTheoThangController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var lstTransaction = from s in db.Transactions
                                 orderby s.ID
                                 select s;
            DateTime dtn = DateTime.Now;
            int monthtemp = dtn.Month;
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    monthtemp = int.Parse(searchString);
                }
                catch (Exception e)
                {

                }
            }
            List<Transaction> lstT = new List<Transaction>();
            int DoanhThu = 0;
            foreach (Transaction t in lstTransaction)
            {
                DateTime x = Convert.ToDateTime(t.CreateDate);
                if (x.Month == monthtemp && x.Year == dtn.Year)
                {
                    DoanhThu += (int)t.Bill;
                    lstT.Add(t);
                }
            }

            ViewBag.ThangThongKe = monthtemp.ToString() + "/" + dtn.Year.ToString();
            ViewBag.DoanhThu = DoanhThu;
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoThang/Index.cshtml",lstT.ToPagedList(pageNumber, pageSize));
        }


        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        int ThongKeTheoTuan(DateTime input)
        {
            int tkT = 0;
            DateTime d = getMonday(input);
            for (int i = 0; i < 7; i++)
            {
                tkT += ThongKeTheoNgay(d.AddDays(i));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
        ////////////////////////////////////////////////////////////
        // GET: /ThongKe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoThang/Details.cshtml", transaction);
        }

        // GET: /ThongKe/Create
    }

    [Authorize(Roles = "Admin")]
    public class ThongKeTheoTuanController : Controller
    {
        //
        // GET: /ThongKeTheoTuan/
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var lstTransaction = from s in db.Transactions
                                 orderby s.ID
                                 select s;
            DateTime dtn = DateTime.Now;
            ViewBag.CurrentSort = sortOrder;
            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                try
                {
                    dtn = Convert.ToDateTime(searchString);
                }
                catch (Exception e)
                {

                }
            }
            List<Transaction> lstT = new List<Transaction>();
            for (int i = 0; i < 7; i++)
            {
                DateTime temp = getMonday(dtn).AddDays(i);
                foreach (Transaction t in lstTransaction)
                {
                    DateTime x = Convert.ToDateTime(t.CreateDate);
                    if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                    {
                        lstT.Add(t);
                    }
                }
            }
            ViewBag.NgayThongKe = getMonday(dtn).ToString();
            ViewBag.NgayThongKe1 = getMonday(dtn).AddDays(6).ToString();
            ViewBag.DoanhThu = ThongKeTheoTuan(dtn);
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoTuan/Index.cshtml", lstT.ToPagedList(pageNumber, pageSize));
        }


        int ThongKeTheoNgay(DateTime x)
        {
            int price = 0;
            foreach (Transaction t in db.Transactions)
            {
                DateTime temp = Convert.ToDateTime(t.CreateDate);
                if (temp.Day == x.Day && temp.Month == x.Month && temp.Year == x.Year)
                {
                    price += (int)t.Bill;
                }
            }
            return price;
        }
        int ThongKeTheoTuan(DateTime input)
        {
            int tkT = 0;
            DateTime d = getMonday(input);
            for (int i = 0; i < 7; i++)
            {
                tkT += ThongKeTheoNgay(d.AddDays(i));
            }
            return tkT;
        }
        // GET: /Transaction/Details/5

        public static DateTime getMonday(DateTime d)
        {
            switch (d.DayOfWeek.ToString())
            {
                case "Monday": return d;
                case "Tuesday": return d.AddDays(-1);
                case "Wednesday": return d.AddDays(-2);
                case "Thursday": return d.AddDays(-3);
                case "Friday": return d.AddDays(-4);
                case "Saturday": return d.AddDays(-5);
                default:
                    return d.AddDays(-6);
            }
        }
        ////////////////////////////////////////////////////////////
        // GET: /ThongKe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/ThongKeDoanhSo/ThongKeTheoTuan/Details.cshtml",transaction);
        }

        // GET: /ThongKe/Create


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}