﻿using ShopOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TopTenController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();
        // GET: TopTen
        public ActionResult Index()
        {
            List<Product> lsTopTen = new List<Product>();
            var fashionMenu = from f in db.FashionTypes
                              select f;
            ViewBag.FashionMenu = fashionMenu.ToList();
            var cart = from c in db.Carts
                          select c;
            foreach(var item in cart)
            {
                item.Product.Stock = item.Quantity;
                int index = lsTopTen.BinarySearch(item.Product, new ProductComparer());
                if (index < 0)
                {
                    lsTopTen.Add(item.Product);
              
                }
                else
                {
                    lsTopTen[index].Stock += item.Quantity;
                }
            }

            return View(lsTopTen.OrderByDescending(item => item.Stock).ToList());
        }
    }

    public class ProductComparer : IComparer<Product>
    {
        public int Compare(Product x, Product y)
        {
            // TODO: Handle x or y being null, or them not having names
            return x.ID.CompareTo(y.ID);
        }
    }
}