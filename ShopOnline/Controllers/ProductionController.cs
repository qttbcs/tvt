﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;
using System.IO;


namespace ShopOnline.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductionController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();

        // GET: /Production/
        public ViewResult Index(string sortOrder)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.DescriptionlSortParm = sortOrder == "Description" ? "description_desc" : "Description";
            ViewBag.StockSortParm = sortOrder == "Stock" ? "stock_desc" : "Stock";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "price_desc" : "Price";
            ViewBag.CategorySortParm = sortOrder == "Category" ? "category_desc" : "Category";
           
            var productions = from s in db.Products
                              select s;

            switch (sortOrder)
            {
                case "name_desc":
                    productions = productions.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    productions = productions.OrderBy(s => s.TimeUpdate);
                    break;
                case "date_desc":
                    productions = productions.OrderByDescending(s => s.TimeUpdate);
                    break;
                case "Description":
                    productions = productions.OrderBy(s => s.Description);
                    break;
                case "description_desc":
                    productions = productions.OrderByDescending(s => s.Description);
                    break;
                case "Stock":
                    productions = productions.OrderBy(s => s.Stock);
                    break;
                case "stock_desc":
                    productions = productions.OrderByDescending(s => s.Stock);
                    break;
                case "Price":
                    productions = productions.OrderBy(s => s.Price);
                    break;
                case "price_desc":
                    productions = productions.OrderByDescending(s => s.Price);
                    break;
                case "Category":
                    productions = productions.OrderBy(s => s.Category.Name);
                    break;
                case "category_desc":
                    productions = productions.OrderByDescending(s => s.Category.Name);
                    break;
                default:
                    productions = productions.OrderBy(s => s.Name);
                    break;
            }

            return View(productions.ToList());

        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            var images = from i in db.ProductImages
                         where i.ProductID == id
                         select i;
            var sizes = from s in db.Sizes
                        where s.ProductID == id
                        select s;
            Session["lm"] = images;
            Session["ls"] = sizes;
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: /Production/Create
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name");
            return View();
        }

        // POST: /Production/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,CategoryID,Description,Stock,Price,TimeUpdate")] Product product, string size1, string size2, string size3, string size4, string size5, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2, HttpPostedFileBase file3)
        {
            //Thêm dữ liệu ảnh
            if (ModelState.IsValid)
            {
                List<string> ls = new List<string>();
                List<string> limg = new List<string>();
                CreateListSize(ls, size1);
                CreateListSize(ls, size2);
                CreateListSize(ls, size3);
                CreateListSize(ls, size4);
                CreateListSize(ls, size5);

                //Mycode 
                foreach (string s in ls)
                {
                    Size st = new Size();
                    st.ID = db.Sizes.ToList()[db.Sizes.ToList().Count - 1].ID + 1;
                    st.ProductID = product.ID;
                    st.Value = s;
                    db.Sizes.Add(st);
                }

                //Create image
                CreateImage(product, file, "1");
                CreateImage(product, file1, "2");
                CreateImage(product, file2, "3");
                CreateImage(product, file3, "4");
                product.NumView = 0;
                product.TimeUpdate = DateTime.Now;
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        void CreateImage([Bind(Include = "ID,Name,CategoryID,Description,Stock,Price,TimeUpdate")] Product product, HttpPostedFileBase file, string num)
        {
            if (file != null && file.ContentLength > 0)
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
                String rootPath = Server.MapPath("~/Content/ShopOnline/images/");
                string sn = (db.Products.ToList()[db.Products.ToList().Count - 1].ID + 1).ToString();
                string s = "P" + sn + "T" + num;
                image.Save(@rootPath + s + ".jpg");
                ProductImage pimg = new ProductImage();
                pimg.ID = db.ProductImages.ToList()[db.ProductImages.ToList().Count - 1].ID + 1;
                pimg.ProductID = product.ID;
                pimg.URL = "~/Content/ShopOnline/images/" + s + ".jpg";
                pimg.Type = int.Parse(num);
                db.ProductImages.Add(pimg);
            }
        }

        void CreateListSize(List<string> l, string s)
        {
            if (s != null)
                l.Add(s);
        }




        // GET: /Production/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            var images = from i in db.ProductImages
                         where i.ProductID == product.ID
                         select i;
            var sizes = from i in db.Sizes
                        where i.ProductID == id
                        select i;
            Session["ls"] = sizes;
            Session["lm"] = images;

            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        // POST: /Production/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,CategoryID,Description,Stock,Price,TimeUpdate")] Product product, string size1, string size2, string size3, string size4, string size5, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2, HttpPostedFileBase file3)
        {
            if (ModelState.IsValid)
            {
                product.TimeUpdate = DateTime.Now;
                db.Entry(product).State = EntityState.Modified;
                List<string> ls = new List<string>();
                CreateListSize(ls, size1);
                CreateListSize(ls, size2);
                CreateListSize(ls, size3);
                CreateListSize(ls, size4);
                CreateListSize(ls, size5);
                var sizes = from s in db.Sizes
                            where s.ProductID == product.ID
                            select s;


                DeleteImage(file, 1, product);
                DeleteImage(file1, 2, product);
                DeleteImage(file2, 3, product);
                DeleteImage(file3, 4, product);

                //Delete
                foreach (Size s in sizes)
                {
                    db.Sizes.Remove(s);
                }

                //Add size
                foreach (string s in ls)
                {
                    Size st = new Size();
                    st.ID = db.Sizes.ToList()[db.Sizes.ToList().Count - 1].ID + 1;
                    st.ProductID = product.ID;
                    st.Value = s;
                    db.Sizes.Add(st);
                }

                //Add image
                if (file != null)
                    CreateImage(product, file, "1");
                if (file1 != null)
                    CreateImage(product, file1, "2");
                if (file2 != null)
                    CreateImage(product, file2, "3");
                if (file3 != null)
                    CreateImage(product, file3, "4");
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        public void DeleteImage(HttpPostedFileBase file, int x, [Bind(Include = "ID,Name,CategoryID,Description,Stock,Price,TimeUpdate")] Product product)
        {
            var images = from i in db.ProductImages
                         where i.ProductID == product.ID
                         select i;

            if (file != null)
            {
                foreach (ProductImage s in images)
                {
                    if (s.Type == x)
                    {
                        db.ProductImages.Remove(s);
                        return;
                    }
                }
            }
        }
        // GET: /Production/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            var images = from i in db.ProductImages
                         where i.ProductID == id
                         select i;
            var sizes = from s in db.Sizes
                        where s.ProductID == id
                        select s;
            Session["lm"] = images;
            Session["ls"] = sizes;
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: /Production/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Code
            Product product = db.Products.Find(id);
            var sizes = from s in db.Sizes
                        where s.ProductID == id
                        select s;
            var images = from img in db.ProductImages
                         where img.ProductID == id
                         select img;

            foreach (ProductImage img in images)
            {
                string fullPath = Request.MapPath(@img.URL);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                db.ProductImages.Remove(img);
            }
            foreach (Size s in sizes)
            {
                db.Sizes.Remove(s);
            }
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
