﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopOnline.Models;
using PagedList;

namespace ShopOnline.Controllers
{
    public class ItemsController : Controller
    {
        private QLShopOnlineEntities db = new QLShopOnlineEntities();



        public ActionResult Index(int? id, int? page, int? currentFilter, int? sortOrder, string ItemName, string PriceFrom, string PriceTo, string currentFilterP, string Size)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FashionType type = db.FashionTypes.Find(id);
            if (type == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.title = type.Name;
            }

            //Search


            if (ItemName != null)
            {
                page = 1;
            }
            else
            {
                ItemName = currentFilterP;
            }


            ViewBag.CurrentFilterP = ItemName;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = currentFilter;
            //
            var lsCategory = from c in db.Categories
                             where c.FashionType == id
                             select c;
            var lsProduct = from p in db.Products
                            where lsCategory.Contains(p.Category)
                            select p;
            var lsProducttemp = new List<Product>();
            lsProducttemp = lsProduct.ToList();
            if (currentFilter != null && currentFilter != -1)
            {
                lsProduct = lsProduct.Where(p => p.CategoryID == currentFilter);
            }

            if (!String.IsNullOrEmpty(ItemName) || !String.IsNullOrEmpty(PriceFrom) || !String.IsNullOrEmpty(PriceTo))
            {
                lsProducttemp = new List<Product>();
                try
                {
                    lsProducttemp = SearchItem(ItemName, PriceFrom, PriceTo, Size, lsCategory.ToList());
                }
                catch (Exception e)
                {
                    lsProducttemp = new List<Product>();
                }
            }

            switch (sortOrder)
            {
                case 1:
                    lsProduct = lsProduct.OrderByDescending(p => p.Ratings.Count());
                    lsProducttemp = lsProduct.ToList();
                    break;
                case 2:

                    break;
                case 3:
                    lsProduct = lsProduct.OrderBy(p => p.Price);
                    lsProducttemp = lsProduct.ToList();
                    break;
                case 4:
                    lsProduct = lsProduct.OrderByDescending(p => p.Price);
                    lsProducttemp = lsProduct.ToList();
                    break;
                case 5:
                    lsProduct = lsProduct.OrderByDescending(p => p.Stock);
                    lsProducttemp = lsProduct.ToList();
                    break;
                default:  // Name ascending 
                    lsProduct = lsProduct.OrderBy(p => p.ID);
                    if (String.IsNullOrEmpty(ItemName) && String.IsNullOrEmpty(PriceFrom) && String.IsNullOrEmpty(PriceTo))
                        lsProducttemp = lsProduct.ToList();
                    break;
            }
            //
            Session["Category"] = lsCategory;
            int pageSize = 6;
            int pageNumber = (page ?? 1);
            return View(lsProducttemp.ToPagedList(pageNumber, pageSize));
        }


        private List<Product> SearchItem(string ItemName, string PriceFrom, string PriceTo, string Size, List<Category> categories)
        {
            List<Product> lstI = new List<Product>();
            List<Product> lstResult = new List<Product>();
            ConvertVN c = new ConvertVN();
            if (ItemName != "")
            {
                if (PriceFrom != "" && PriceTo != "")
                {
                    foreach (Product p in db.Products)
                    {
                        if (c.LocDau(p.Name).ToLower().Contains(ItemName.ToLower()) && p.Price >= int.Parse(PriceFrom) && int.Parse(PriceTo) >= p.Price)
                            lstI.Add(p);
                    }
                }
                else if (PriceTo != "" && PriceFrom == "")
                {
                    foreach (Product p in db.Products)
                    {
                        if (c.LocDau(p.Name).ToLower().Contains(ItemName.ToLower()) && p.Price >= 0 && int.Parse(PriceTo) >= p.Price)
                            lstI.Add(p);
                    }
                }
                else if (PriceTo == "" && PriceFrom != "")
                {
                    foreach (Product p in db.Products)
                    {
                        if (c.LocDau(p.Name).ToLower().Contains(ItemName.ToLower()) && p.Price >= int.Parse(PriceFrom))
                            lstI.Add(p);
                    }
                }
                else
                {
                    foreach (Product p in db.Products)
                    {
                        if (c.LocDau(p.Name).ToLower().Contains(ItemName.ToLower()))
                            lstI.Add(p);
                    }
                }
            }
            else
            {
                if (PriceFrom != "" && PriceTo != "")
                {
                    foreach (Product p in db.Products)
                    {
                        if (p.Price >= int.Parse(PriceFrom) && int.Parse(PriceTo) >= p.Price)
                            lstI.Add(p);
                    }
                }
                else if (PriceTo != "" && PriceFrom == "")
                {
                    foreach (Product p in db.Products)
                    {
                        if (p.Price >= 0 && int.Parse(PriceTo) >= p.Price)
                            lstI.Add(p);
                    }
                }
                else
                {
                    foreach (Product p in db.Products)
                    {
                        if (p.Price >= int.Parse(PriceFrom))
                            lstI.Add(p);
                    }
                }
            }
            //Phan loai
            foreach (Product p in lstI)
            {
                if (checkCategory(categories, (int)p.CategoryID))
                    lstResult.Add(p);
            }

            if (Size != null)
            {
                List<Product> listP1 = new List<Product>();
                if (PriceFrom != "" || PriceTo != "" || ItemName != "")
                {
                    foreach (Product p in lstResult)
                    {
                        if (checkSize(p.Sizes.ToList(), Size))
                            listP1.Add(p);
                    }
                }
                else
                {
                    foreach (Product p in db.Products)
                    {
                        if (checkSize(p.Sizes.ToList(), Size))
                            listP1.Add(p);
                    }
                }
                lstResult = listP1;
            }


            return lstResult;
        }



        bool checkCategory(List<Category> x, int input)
        {
            foreach (Category c in x)
            {
                if (c.ID == input)
                    return true;
            }
            return false;
        }

        bool checkSize(List<Size> x, String input)
        {
            foreach (Size s in x)
            {
                string size = s.Value.Split(' ')[0];
                if (size == input)
                    return true;
            }
            return false;
        }

        public ActionResult UpdateView(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = (from p in db.Products
                                where p.ID == id
                                select p).First();
            if (product == null)
            {
                return HttpNotFound();
            }
            else
            {
                product.NumView++;             
                db.SaveChanges();
            }     
            return RedirectToAction("Detail", new { id=id});
        }

        public ActionResult UpdateCart(int? id, string size, string stock)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            else
            {
                Cart cart = new Cart();
                cart.Product = product;
                cart.Quantity = int.Parse(stock);
                var lsCart = Session["Carts"] as List<ShopOnline.Models.Cart>;
                bool isContain=false;
                for (int i = 0; i < lsCart.Count; i++ )
                {
                    if(lsCart[i].Product.ID==cart.Product.ID)
                    {
                        isContain=true;
                        lsCart[i].Quantity += cart.Quantity;
                        break;
                    }
                }
                if(!isContain) lsCart.Add(cart);
            }
            return RedirectToAction("Detail", new { id = id });
        }
        
        public ActionResult Detail(int? id, int? page, string msg)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.title = product.Name;
            }
            //
            if (msg!=null && msg!="")
            {
                var user = db.AspNetUsers.FirstOrDefault(i => i.UserName == User.Identity.Name);
                Comment newComment = new Comment();
                newComment.UserID = user.Id;
                newComment.Message = msg;
                newComment.ProductID = product.ID;
                newComment.CreateDate = DateTime.Now;

                db.Comments.Add(newComment);
                db.SaveChanges();
            }
            //
           
            //
            var lsProduct = from p in db.Products
                            where p.Category.FashionType==product.Category.FashionType
                            select p;
            var lsComment = from c in db.Comments
                            where c.ProductID==product.ID
                            select c;
            lsComment = lsComment.OrderByDescending(c => c.CreateDate);
            Session["PreProduct"] = lsProduct;
            //
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ViewBag.comments = lsComment.ToPagedList(pageNumber, pageSize);
            return Request.IsAjaxRequest()
                ? (ActionResult)PartialView("Comments")
                : View(product);
        }


    }
}