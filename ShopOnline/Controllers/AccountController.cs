﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using ShopOnline.Models;
using Newtonsoft.Json;
using System.Net;

namespace ShopOnline.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    if (user.Active==1)
                    {
                        await SignInAsync(user, model.RememberMe);
                        if (UserManager.IsInRole(user.Id,"Admin"))
                            return RedirectToAction("IndexAdmin", "Home");
                        else
                            return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid username or password.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.DKTC = 1;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser(){ UserName = model.UserName,
                                                  FullName = model.FullName,
                                                  Email = model.Email,
                                                  MobileNumber = model.MobileNumber,
                                                  CreateDate = DateTime.Today,
                                                  Active = 0};// Tao bien user luu thong tin
                // doan ma kiem tra captcha
                var response = Request["g-recaptcha-response"];
                const string secret = "6LfgCQgTAAAAAD-1Luv_qO1NPJs0rLuYgmyVni1E";
                var client = new WebClient();
                var reply =
                    client.DownloadString(
                        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
                // /////////
                if (captchaResponse.Success=="True")
                {
                    var result = await UserManager.CreateAsync(user, model.Password); // tao tai khoang ham cua Identity
                    if (result.Succeeded)
                    {
                        ViewBag.DKTC = 2;
                        UserManager.AddToRole(user.Id, "Member");/////Them quyen cho tai khoan la: member
                        SendMail(user.Id, user.Email, "/Account/ConfirmAccount?active=", "Clink to confirm your registration", "Active Accounts");
                     
                    }
                    else
                    {
                        ViewBag.DKTC = 3;
                        AddErrors(result);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //Send mail confirm account
        public bool SendMail(String IDUser, string email, string url, string textclick, string subject)
        {
            string userMailSend = "thanhvongspw@gmail.com";
            string passMailSend = "thanhvongspw01234";
            var hosturl = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) +
                        url + IDUser;
            var confirmationLink = string.Format("<a href=\"{0}\">"+textclick+"</a>",
                                                 hosturl); // Noi dung Link kich hoat tai khoan
            //doan code gui mail 
            string smtpHost = "smtp.gmail.com";
            int smtpPort = 587;
            EmailService service = new EmailService();
            bool resultSend = service.Send(userMailSend, passMailSend, smtpHost, smtpPort, email, subject, confirmationLink);
            if (resultSend) ModelState.AddModelError("", "Send mail successful");
            else ModelState.AddModelError("", "Send mail unsuccessful");
            //
            return resultSend;
        }
        //////////////////////////


        //Confirm Accounts
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmAccount(string ID)
        {
            var AccountsActive = Request["active"];
            if (!String.IsNullOrEmpty(AccountsActive))
            {
                var user = await UserManager.FindByIdAsync(AccountsActive);//tim user bang id
                if (user != null)
                {
                    user.Active = 1;// cap nhat active=1 : da kich hoat accounts
                    var result = await UserManager.UpdateAsync(user);//update data
                    await SignInAsync(user, false);// dang nhap
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }
        //

        //Forgot password
        [AllowAnonymous]
        public ActionResult RecoverPassword()
        {
            return View();
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RecoverPassword(string UserName)
        {
            if (UserName != null)
            {
                var user = await UserManager.FindByNameAsync(UserName);
                if (user != null)
                {
                    SendMail(user.Id, user.Email, "/Account/RecoverPasswordFromMail?fg=", "Clink to Recover Password","Recover Password");
                }
            }
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> RecoverPasswordFromMail(string ID)
        {
            var AccountsActive = Request["fg"];
            if (!String.IsNullOrEmpty(AccountsActive))
            {
                var user = await UserManager.FindByIdAsync(AccountsActive);//tim user bang id
                if (user != null)
                {
                    Session["UserNameCurrent"] = user.UserName;
                }
                else
                    return RedirectToAction("Index", "Home");
            }
            else 
                return RedirectToAction("Index","Home");
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RecoverPasswordFromMail(ManageUserViewModel model)
        {
            string username = (string)Session["UserNameCurrent"];// lay du lieu session user name hien tai
            
                if (model != null && username != null)
                {
                    var user = await UserManager.FindByNameAsync(username);//tim user bang id
                    await UserManager.RemovePasswordAsync(user.Id);// xoa di mat khau cu da luu
                    IdentityResult result =  await UserManager.AddPasswordAsync(user.Id, model.NewPassword);// them lai mat khau moi vao
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, true);//dang nhap
                        return RedirectToAction("Index", "Home"); // ve trang chinh
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            return View();
        }
        //
        //
        [AllowAnonymous]
        public ActionResult Details()
        {
            RegisterViewModel inf = new RegisterViewModel();
            string name = User.Identity.Name;
            var user = UserManager.FindByName(name);
            inf.UserName = user.UserName;
            inf.FullName = user.FullName;
            inf.Email = user.Email;
            inf.MobileNumber = user.MobileNumber;

            return View(inf);
        }
        //
        [AllowAnonymous]
        public ActionResult Edit()
        {
            return View();
        }

        [Authorize(Roles = "Admin,Member")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UpdateInfo model)
        {
            if (ModelState.IsValid)
            {
                if (User.Identity.Name != null)
                {
                    var user = UserManager.FindByName(User.Identity.Name);
                    if (!String.IsNullOrWhiteSpace(model.FullName))
                        user.FullName = model.FullName;
                    if (!String.IsNullOrWhiteSpace(model.Email))
                        user.Email = model.Email;
                    if (!String.IsNullOrWhiteSpace(model.MobileNumber))
                        user.MobileNumber = model.MobileNumber;
                    var result = UserManager.Update(user);//update data
                    return RedirectToAction("Details", "Account");
                }
            }
            return View();
        }

        #region Admin

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult AdminManageUser()
        {
            QLShopOnlineEntities db = new QLShopOnlineEntities();
            var users = from u in db.AspNetUsers select u;
            List<UserAndRole> uar = new List<UserAndRole>();
            foreach(var item in users)
            {
                UserAndRole t = new UserAndRole();
                t.UserName = item.UserName;
                t.CreateDate = item.CreateDate.ToString();
                t.Role = UserManager.GetRoles(item.Id).First();
                t.Status = (item.Active == 1) ? "Actived" : "Pending";
                uar.Add(t);
            }
            return View(uar);
        }

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult DetailUsers(string username)
        {
            if(username==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var user =  UserManager.FindByName(username);
            InfoUsers t = new InfoUsers();
            t.UserName = user.UserName;
            t.FullName = user.FullName;
            t.Email = user.Email;
            t.NumberPhone = user.MobileNumber;
            t.Role = UserManager.GetRoles(user.Id).First();
            return View(t);
        }

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public ActionResult EditUsers(string username)
        {
            if (username == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var user = UserManager.FindByName(username);
            InfoUsers t = new InfoUsers();
            t.UserName = user.UserName;
            t.FullName = user.FullName;
            t.Email = user.Email;
            t.NumberPhone = user.MobileNumber;
            t.Role = UserManager.GetRoles(user.Id).First();
            return View(t);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(InfoUsers infouser)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(infouser.UserName);
                if (user != null)
                {
                    user.FullName = infouser.FullName;
                    user.Email = infouser.Email;
                    user.MobileNumber = infouser.NumberPhone;
                    ClearUserRoles(user.Id);
                    UserManager.AddToRole(user.Id, infouser.Role);
                    var result = UserManager.Update(user);//update data
                    return RedirectToAction("IndexAdmin", "Home");
                }
            }
            return View();
        }

        public void ClearUserRoles(string userId)
        {
            var user = UserManager.FindById(userId);
            var currentRoles = new List<IdentityUserRole>();

            currentRoles.AddRange(user.Roles);
            foreach (var role in currentRoles)
            {
                UserManager.RemoveFromRole(userId, role.Role.Name);
            }
        } // Xóa UserRoles

        [Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public async Task<ActionResult> DeleteUsers(string username)
        {
            if (username != null && username!=User.Identity.Name)
            { 
                var user = UserManager.FindByName(username);
                ClearUserRoles(user.Id);
                QLShopOnlineEntities db = new QLShopOnlineEntities();
                AspNetUser t = db.AspNetUsers.Find(user.Id);
                db.AspNetUsers.Remove(t);
                db.SaveChanges();
                
                return RedirectToAction("AdminManageUser", "Account");
            }
            return View();
        }
        #endregion



        /////////////////////////////////////////////////////////////////////////
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [Authorize(Roles = "Admin,Member")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}